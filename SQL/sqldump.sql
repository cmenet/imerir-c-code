-- MySQL dump 10.14  Distrib 5.5.34-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	5.5.34-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aliment`
--

DROP TABLE IF EXISTS `aliment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aliment` (
  `a_id` int(11) NOT NULL AUTO_INCREMENT,
  `a_nom` char(30) NOT NULL,
  PRIMARY KEY (`a_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aliment`
--

LOCK TABLES `aliment` WRITE;
/*!40000 ALTER TABLE `aliment` DISABLE KEYS */;
INSERT INTO `aliment` VALUES (1,'Banana'),(2,'Viande Boeuf'),(3,'Farce'),(4,'Pomme de terre'),(5,'Oignon'),(6,'riz'),(7,'Viande de Lama'),(8,'Lama en sauce');
/*!40000 ALTER TABLE `aliment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aliment_stocke`
--

DROP TABLE IF EXISTS `aliment_stocke`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aliment_stocke` (
  `e_id` char(30) NOT NULL,
  `f_id` char(30) NOT NULL,
  `a_id` int(11) NOT NULL,
  `as_quantite` int(11) DEFAULT NULL,
  `as_unite` enum('GRAMMES','KILO','LITRE','UNITE') DEFAULT NULL,
  `as_date_achat` date NOT NULL,
  `as_date_peremption` date NOT NULL,
  `as_type_conservation` enum('SURGELE','PRODUIT_FRAI','CONSERVE') DEFAULT NULL,
  PRIMARY KEY (`e_id`,`f_id`,`a_id`),
  KEY `f_id` (`f_id`,`e_id`),
  KEY `a_id` (`a_id`),
  CONSTRAINT `aliment_stocke_ibfk_1` FOREIGN KEY (`f_id`, `e_id`) REFERENCES `etagere` (`f_id`, `e_id`),
  CONSTRAINT `aliment_stocke_ibfk_2` FOREIGN KEY (`a_id`) REFERENCES `aliment` (`a_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aliment_stocke`
--

LOCK TABLES `aliment_stocke` WRITE;
/*!40000 ALTER TABLE `aliment_stocke` DISABLE KEYS */;
INSERT INTO `aliment_stocke` VALUES ('A','FRIGO 1',1,3,NULL,'0000-00-00','0000-00-00','SURGELE'),('A','FRIGO 1',2,1,'KILO','2014-02-10','0000-00-00','PRODUIT_FRAI'),('A','FRIGO 1',5,30,'UNITE','2014-02-10','0000-00-00','PRODUIT_FRAI'),('A','FRIGO 1',8,800,'GRAMMES','2014-02-10','0000-00-00','PRODUIT_FRAI'),('A','FRIGO 4',7,20,'KILO','2014-02-10','0000-00-00','CONSERVE'),('B','FRIGO 3',3,600,'GRAMMES','2014-02-09','2014-02-20','PRODUIT_FRAI'),('B','FRIGO 4',4,10,'KILO','2014-02-10','0000-00-00','SURGELE'),('B','FRIGO 4',7,2,'LITRE','2014-02-08','0000-00-00','PRODUIT_FRAI'),('C','FRIGO 2',1,10,'UNITE','2014-02-10','0000-00-00','PRODUIT_FRAI'),('C','FRIGO 2',6,2,'KILO','2014-02-10','2014-02-14','PRODUIT_FRAI');
/*!40000 ALTER TABLE `aliment_stocke` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etagere`
--

DROP TABLE IF EXISTS `etagere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etagere` (
  `e_id` char(30) NOT NULL,
  `f_id` char(30) NOT NULL,
  PRIMARY KEY (`e_id`,`f_id`),
  KEY `f_id` (`f_id`),
  CONSTRAINT `etagere_ibfk_1` FOREIGN KEY (`f_id`) REFERENCES `frigo` (`f_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etagere`
--

LOCK TABLES `etagere` WRITE;
/*!40000 ALTER TABLE `etagere` DISABLE KEYS */;
INSERT INTO `etagere` VALUES ('A','FRIGO 1'),('A','FRIGO 2'),('A','FRIGO 3'),('A','FRIGO 4'),('B','FRIGO 1'),('B','FRIGO 2'),('B','FRIGO 3'),('B','FRIGO 4'),('C','FRIGO 1'),('C','FRIGO 2'),('C','FRIGO 4'),('D','FRIGO 4');
/*!40000 ALTER TABLE `etagere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frigo`
--

DROP TABLE IF EXISTS `frigo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frigo` (
  `f_id` char(30) NOT NULL,
  PRIMARY KEY (`f_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frigo`
--

LOCK TABLES `frigo` WRITE;
/*!40000 ALTER TABLE `frigo` DISABLE KEYS */;
INSERT INTO `frigo` VALUES ('FRIGO 1'),('FRIGO 2'),('FRIGO 3'),('FRIGO 4');
/*!40000 ALTER TABLE `frigo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredient`
--

DROP TABLE IF EXISTS `ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredient` (
  `a_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  PRIMARY KEY (`a_id`,`p_id`),
  KEY `p_id` (`p_id`),
  CONSTRAINT `ingredient_ibfk_1` FOREIGN KEY (`a_id`) REFERENCES `aliment` (`a_id`),
  CONSTRAINT `ingredient_ibfk_2` FOREIGN KEY (`p_id`) REFERENCES `plat` (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient`
--

LOCK TABLES `ingredient` WRITE;
/*!40000 ALTER TABLE `ingredient` DISABLE KEYS */;
INSERT INTO `ingredient` VALUES (2,4),(4,2),(4,7),(5,1),(5,7),(6,3),(6,8),(7,1),(7,5),(8,3),(8,6);
/*!40000 ALTER TABLE `ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `m_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_nom` char(30) NOT NULL,
  PRIMARY KEY (`m_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'lama pour enfants'),(2,'lundi c\'est lama !'),(3,'Carnivore'),(4,'lamavore'),(5,'Menu d\'a coté !');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plat`
--

DROP TABLE IF EXISTS `plat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plat` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_nom` char(30) NOT NULL,
  `p_type` enum('ENTRE','PLAT','DESSERT') DEFAULT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plat`
--

LOCK TABLES `plat` WRITE;
/*!40000 ALTER TABLE `plat` DISABLE KEYS */;
INSERT INTO `plat` VALUES (1,'Lama sur son lit d\'oignons','PLAT'),(2,'Salade de pomme de terre','ENTRE'),(3,'Petit riz sauce lama','ENTRE'),(4,'Rumsteak de BOEUF 2 kilos','DESSERT'),(5,'Lama','PLAT'),(6,'Glace lama','DESSERT'),(7,'Pomme de terre aux oignons','ENTRE'),(8,'Riz','PLAT');
/*!40000 ALTER TABLE `plat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sert`
--

DROP TABLE IF EXISTS `sert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sert` (
  `m_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  PRIMARY KEY (`m_id`,`p_id`),
  KEY `p_id` (`p_id`),
  CONSTRAINT `sert_ibfk_1` FOREIGN KEY (`m_id`) REFERENCES `menu` (`m_id`),
  CONSTRAINT `sert_ibfk_2` FOREIGN KEY (`p_id`) REFERENCES `plat` (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sert`
--

LOCK TABLES `sert` WRITE;
/*!40000 ALTER TABLE `sert` DISABLE KEYS */;
INSERT INTO `sert` VALUES (1,6),(2,1),(2,3),(2,6),(3,2),(3,4),(3,5),(4,1),(4,3),(4,6),(5,7),(5,8);
/*!40000 ALTER TABLE `sert` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-02-27 11:48:04
