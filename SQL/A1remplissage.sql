INSERT INTO frigo (f_id) VALUES ('FRIGO 1');
INSERT INTO frigo (f_id) VALUES ('FRIGO 2');
INSERT INTO frigo (f_id) VALUES ('FRIGO 3');
INSERT INTO frigo (f_id) VALUES ('FRIGO 4');

INSERT INTO etagere (f_id,e_id) VALUES ('FRIGO 1','A');
INSERT INTO etagere (f_id,e_id) VALUES ('FRIGO 1','B');
INSERT INTO etagere (f_id,e_id) VALUES ('FRIGO 1','C');

INSERT INTO etagere (f_id,e_id) VALUES ('FRIGO 2','A');
INSERT INTO etagere (f_id,e_id) VALUES ('FRIGO 2','B');
INSERT INTO etagere (f_id,e_id) VALUES ('FRIGO 2','C');

INSERT INTO etagere (f_id,e_id) VALUES ('FRIGO 3','A');
INSERT INTO etagere (f_id,e_id) VALUES ('FRIGO 3','B');
INSERT INTO etagere (f_id,e_id) VALUES ('FRIGO 3','C');

INSERT INTO etagere (f_id,e_id) VALUES ('FRIGO 4','A');
INSERT INTO etagere (f_id,e_id) VALUES ('FRIGO 4','B');
INSERT INTO etagere (f_id,e_id) VALUES ('FRIGO 4','C');
INSERT INTO etagere (f_id,e_id) VALUES ('FRIGO 4','D');

INSERT INTO aliment VALUES (null,'Bannana');
INSERT INTO aliment VALUES (null,'Viande Boeuf');
INSERT INTO aliment VALUES (null,'Viande de Lama');
INSERT INTO aliment VALUES (null,'Farce');
INSERT INTO aliment VALUES (null,'Pomme de terre');
INSERT INTO aliment VALUES (null,'Oignon');
INSERT INTO aliment VALUES (null,'riz');
INSERT INTO aliment VALUES (null,'Lama en sauce');

INSERT INTO aliment_stocke VALUES ('A','FRIGO 1',2,1,'KILO','2014-02-10','2014-02-30','PRODUIT_FRAI');
INSERT INTO aliment_stocke VALUES ('A','FRIGO 1',8,800,'GRAMMES','2014-02-10','2014-02-30','PRODUIT_FRAI');
INSERT INTO aliment_stocke VALUES ('C','FRIGO 2',1,10,'UNITE','2014-02-10','2014-02-30','PRODUIT_FRAI');
INSERT INTO aliment_stocke VALUES ('B','FRIGO 3',3,600,'GRAMMES','2014-02-9','2014-02-20','PRODUIT_FRAI');
INSERT INTO aliment_stocke VALUES ('B','FRIGO 4',4,10,'KILO','2014-02-10','2015-02-30','SURGELE');
INSERT INTO aliment_stocke VALUES ('A','FRIGO 1',5,30,'UNITE','2014-02-10','2014-02-30','PRODUIT_FRAI');
INSERT INTO aliment_stocke VALUES ('C','FRIGO 2',6,2,'KILO','2014-02-10','2014-02-14','PRODUIT_FRAI');
INSERT INTO aliment_stocke VALUES ('A','FRIGO 4',7,20,'KILO','2014-02-10','2016-02-30','CONSERVE');
INSERT INTO aliment_stocke VALUES ('B','FRIGO 4',7,2,'LITRE','2014-02-08','2016-02-31','PRODUIT_FRAI');


INSERT INTO plat VALUES (null,'Lama sur son lit d\'oignons','PLAT');
INSERT INTO plat VALUES (null,'Salade de pomme de terre','ENTRE');
INSERT INTO plat VALUES (null,'Petit riz sauce lama','ENTRE');
INSERT INTO plat VALUES (null,'Rumsteak de BOEUF 2 kilos','DESSERT');
INSERT INTO plat VALUES (null,'Lama','PLAT');
INSERT INTO plat VALUES (null,'Glace lama','DESSERT');
INSERT INTO plat VALUES (null,'Pomme de terre aux oignons','ENTRE');
INSERT INTO plat VALUES (null,'Riz','PLAT');

INSERT INTO ingredient VALUES (5,1);
INSERT INTO ingredient VALUES (7,1);
INSERT INTO ingredient VALUES (4,2);
INSERT INTO ingredient VALUES (8,3);
INSERT INTO ingredient VALUES (6,3);
INSERT INTO ingredient VALUES (2,4);
INSERT INTO ingredient VALUES (7,5);
INSERT INTO ingredient VALUES (8,6);
INSERT INTO ingredient VALUES (4,7);
INSERT INTO ingredient VALUES (5,7);
INSERT INTO ingredient VALUES (6,8);

INSERT INTO menu VALUES(null,'lama pour enfants');
INSERT INTO menu VALUES(null,'lundi c\'est lama');
INSERT INTO menu VALUES(null,'Carnivore');
INSERT INTO menu VALUES(null,'lamavore');
INSERT INTO menu VALUES(null,'Menu d\'a coté !');


INSERT INTO sert VALUES(1,6);
INSERT INTO sert VALUES(2,3);
INSERT INTO sert VALUES(2,1);
INSERT INTO sert VALUES(2,6);
INSERT INTO sert VALUES(3,2);
INSERT INTO sert VALUES(3,5);
INSERT INTO sert VALUES(3,4);
INSERT INTO sert VALUES(4,3);
INSERT INTO sert VALUES(4,1);
INSERT INTO sert VALUES(4,6);
INSERT INTO sert VALUES(5,7);
INSERT INTO sert VALUES(5,8);




