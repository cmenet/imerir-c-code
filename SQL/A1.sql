

CREATE TABLE menu (
m_id INT AUTO_INCREMENT ,
m_nom CHAR(30) NOT NULL,
PRIMARY KEY (m_id)
);


CREATE TABLE plat(
p_id INT AUTO_INCREMENT ,
p_nom CHAR(30) NOT NULL,
p_type ENUM('ENTRE','PLAT','DESSERT'),
PRIMARY KEY (p_id)
);

CREATE TABLE sert(
m_id INT NOT NULL,
p_id INT NOT NULL,
PRIMARY KEY(m_id,p_id),
FOREIGN KEY (m_id) REFERENCES menu (m_id),
FOREIGN KEY (p_id) REFERENCES plat (p_id)
);

CREATE TABLE aliment(
a_id INT AUTO_INCREMENT,
a_nom char(30) NOT NULL,
PRIMARY KEY(a_id)
);

CREATE TABLE ingredient(
a_id INT NOT NULL,
p_id INT NOT NULL,
PRIMARY KEY(a_id,p_id),
FOREIGN KEY (a_id) REFERENCES aliment (a_id),
FOREIGN KEY (p_id) REFERENCES plat (p_id)
);

CREATE TABLE frigo(
f_id char(30) NOT NULL,
PRIMARY KEY (f_id)
);

CREATE TABLE etagere(
e_id char(30) NOT NULL,
f_id char(30) NOT NULL,
PRIMARY KEY(e_id,f_id),
FOREIGN KEY(f_id) REFERENCES frigo(f_id)
);


CREATE TABLE aliment_stocke(
e_id char(30) NOT NULL,
f_id char(30) NOT NULL,
a_id INT NOT NULL,
as_quantite INT,
as_unite ENUM('GRAMMES','KILO','LITRE','UNITE'),
as_date_achat DATE NOT NULL,
as_date_peremption DATE NOT NULL,
as_type_conservation ENUM('SURGELE','PRODUIT_FRAI','CONSERVE'),
PRIMARY KEY(e_id,f_id,a_id),
FOREIGN KEY(f_id,e_id) REFERENCES etagere(f_id,e_id),
FOREIGN KEY (a_id) REFERENCES aliment(a_id)
);






















