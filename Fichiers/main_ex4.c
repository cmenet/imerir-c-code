#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

typedef struct Etudiant Etudiant;

struct Etudiant
{
	int identifiant;
	char* nom;
	char* prenom;
	int* note;
	int nb_note;
	
};

void F_Ecrit(FILE *f, int nb_ent, Etudiant *tabEtu)
{
	int i;
	
	int size;
	

	fseek(f,0,SEEK_END);
	printf("res ftell : %d", ftell(f) );
	fseek(f,0,SEEK_SET);	

	/*Nombre d'entrées*/
	printf("\n%ld\n",sizeof(nb_ent));
	fwrite(&nb_ent,sizeof(int),1,f);
	
    	for(i=0; i < nb_ent; i++)
	{
		
		/*identifiant*/
        	fwrite(&tabEtu[i].identifiant,sizeof(int),1,f);

		/*taille nom*/
		size = strlen(tabEtu[i].nom)+1;
		fwrite(&size,sizeof(int),1,f);

		/*nom*/
        	fwrite(tabEtu[i].nom,sizeof(char),size,f);

		/*taille prenom*/
		size = strlen(tabEtu[i].prenom)+1;
		fwrite(&size,sizeof(int),1,f);

		/*prenom*/
        	fwrite(tabEtu[i].prenom,sizeof(char),size,f);

		/*nb notes*/
		fwrite(&tabEtu[i].nb_note,sizeof(int),1,f);

		/*notes*/
        	fwrite(tabEtu[i].note,sizeof(int),tabEtu[i].nb_note,f);

	}
	
}

Etudiant* F_Lire (FILE*f, int *nb_ent) /* renvoie le nombre d'entrées lues*/
{
	int i;
	int j;
	
	int size;

	int test;

	Etudiant* tabEtu = NULL;

	
	
	/*Nombre d'entrées*/
	size = fread(nb_ent,sizeof(int),1,f);
	printf("nb ent : %d \n",*nb_ent);

	tabEtu = malloc(sizeof(Etudiant) * (*nb_ent));

	for(i=0; i < *nb_ent; i++)
	{
		
		/*identifiant*/
		fread(&tabEtu[i].identifiant,sizeof(int),1,f);
		printf("identifiant : %ld\n",tabEtu[i].identifiant);

		/*taille nom*/
		
        	fread(&size,sizeof(int),1,f);
		tabEtu[i].nom = malloc(sizeof(char)*size);
		printf("size nom : %d\n",size);
		

		/*nom*/
        	test = fread(tabEtu[i].nom,sizeof(char),size,f);
		printf("test : %d\n",test);
		printf("errno %s\n", strerror(errno));

		for(j = 0; j<size; j++ )
			printf("j : %d -> %c\n",j,tabEtu[i].nom[j]);		

		printf("nom : %s\n",tabEtu[i].nom);

		/*taille prenom*/
		fread(&size,sizeof(int),1,f);
		tabEtu[i].prenom = malloc(sizeof(char)*size);
		printf("size prenom : %d\n",size);

		/*prenom*/
        	fread(tabEtu[i].prenom,sizeof(char),size,f);
		printf("prenom : %s\n",tabEtu[i].prenom);

		/*nb notes*/
		fread(&tabEtu[i].nb_note,sizeof(int),1,f);
		printf("nb notes : %d\n", tabEtu[i].nb_note);

		tabEtu[i].note = malloc(sizeof(int) * tabEtu[i].nb_note);

		/*notes*/
        	fread(tabEtu[i].note,sizeof(int),tabEtu[i].nb_note,f);

	}
	

	return tabEtu;
		

	
}

void Affiche(Etudiant* E)
{
	int i;	
	printf("Affiche\n");
	printf("id : %d \n", E->identifiant);
	printf("nom : %s \n", E->nom);
	printf("prenom : %s \n", E->prenom);
	
	for( i = 0 ; i < E -> nb_note; i++)
	printf("\tnotes[%d] : %d \n", i,E->note[i]);
}


int main (int argc, char** argv)
{
	FILE* fichier = NULL;
	Etudiant E;
	Etudiant* R;
	char* prenom = "Test";
	char* nom = "Test2";

	int nb_etu;

	int note[5] = {10,15,14,16,18};
	
	

	

	
	E.identifiant = 3;
	E.nom = nom;
	E.prenom = prenom;
	E.note = note;
	E.nb_note = 5;


	
    	fichier = fopen("TEST_EX_4","wb+") ;

	if (fichier != NULL)
	{
		printf("Ecriture\n");
		F_Ecrit(fichier, 1, &E);

		fclose(fichier);
	}


	fichier = fopen("TEST_EX_4","rb") ;
	if (fichier != NULL)
	{
		printf("errno %s\n", strerror(errno));
		printf("Lecture\n");
		R = F_Lire(fichier,&nb_etu);



		fclose(fichier);
	}

	// on affiche l'étudiant lu
	Affiche(R);


	return 0;

	/*
	FILE* fichier;
	FILE* f_out ;

	
	char s[100];
	int i;


	if(argc == 3)
	{
		
		fichier = fopen(argv[1],"r") ;
		
		if(fichier != NULL)
		{
			i = 1;
			while(fgets(s,100,fichier))
			{
				if(strstr(s,argv[2]) != NULL)
				printf("%d\n",i);

				i++;
				
			}
		
			fclose(fichier);
			return 0;
		}
		else
			printf("Fichier introuvable");
	}
	else
		printf("Nombre d'arguments invalides\n");*/
}
//ABCDE abcde


