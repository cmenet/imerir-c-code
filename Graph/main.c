#include <stdio.h>

#define n_max 10

int M[n_max][n_max], n, m ;
   // Ligne    Colonne
/* n_max est le nombre maximum de sommets qu’on peut saisir.
    n = le nombre de sommets effectifs.
    m = le nombre d’arcs/Flèches effectif */

void saisie ()
{
	int i, j,ext_i, ext_t, c;
	/* i, j pour les boucles.
	i parcours la ligne de M.
	j parcours la colonne de M.
	Un arc = ( ext_i, ext_t)
	c = Le coût de l’arc */
	/* saisie de n et m */
	printf ("entre stp 2 entiers pour n et m \n");
	scanf ("%d%d", &n, &m);
	/* Tapez 57 */
	/* INITIALISATION de la matrice M avec sommets
	0 arcs. M =     [∞ ∞ ∞ ∞ … ∞]
	[∞ ∞ ∞ ∞ … ∞]
	[∞ ∞ ∞ ∞ … ∞]
	[∞ ∞ ∞ ∞ … ∞] */

	for (i= 1; i<= n; i++)
	    for (j=1; j<= n ; j++)
		if(i==j)
			M[i][j] = 0;
		else 
			M[i][j] = 1000;
	/* fin d’initialisation */
	/* saisie des arcs */
	for (i=1; i<= m ; i++)
	{
	    printf("entres stp (...) l’arc numéro %d \n", i);
	    scanf("%d%d"	, &ext_i, &ext_t);
	    printf("entres stp (...) le coût de l’arc %d \n", i);
	    scanf("%d", &c);
	    M[ext_i][ext_t]=c;
	}
}
void affichage()
{
    int i, j;
    for( i=1; i<= n ; i++)
	{
		for( j=1 ; j<=n ; j++)
		    printf(" %6d ", M[i][j]);
		printf("\n\n");
    	}
}

int main()
{
	saisie();
	affichage();

	return 1;
}

