#include <stdio.h>
#include <stdlib.h>

# define n_max 10

struct sommet

{ 

	int v;

	struct sommet * suiv;

};

/* v suiv v, sui sont 2 champs de stucts */


struct sommet *succ[n_max];

int n, m ;

void saisie ()

{

	int i, ext_i, ext_t, n,m;

	struct sommet *t;

	/* saisie de n et m */

	printf ("entres stp 2 entiers pour n et m \n");

	scanf ("%d %d", &n, &m);

	/* Initialisation de tableau succ avec n sommets et 0 arcs :

	[¨¨|¨¨]->NULL 

	[¨¨|¨¨]->NULL 

	[¨¨|¨¨]->NULL 

	[¨¨|¨¨]->NULL */

	for( i= 1 ; i <= n ; i++)
		succ[i] = NULL ;


	/* fin d’initialisation */

	/*saisie des arcs */

	for ( i = 1; i <= m ; i ++)

	{

		printf ("entres stp l’arc numéro %d \n" , i);

		scanf ( "%d%d", & ext_i, & ext_t);

		/* créer un sommet t |--> [¨¨|¨¨]*/

		t= (struct sommet * ) malloc ( sizeof (struct sommet * ));

		/*Attacher le noeud [¨¨¨¨¨¨|¨¨] à la tête de la liste succ[ext_i] */

		t -> v = ext_t ;

		t -> suiv = succ[ext_i];

		/* fin for */
	}
}

void affichage()
{

	struct sommet *t;
	int i;

	for ( i = 1 ; i<= n ; i++ )
	{

		/* Afficher les succ[i] */

		t = succ[i];

		if ( t == NULL)
		printf( " pas de succ de %d \n" , i);

		else
		{

			while (t != NULL)
			{
				printf ( "%d", t->v);
				t = t -> suiv;
			}

			printf ( "\n");

			}

		}

}


int main()
{
	saisie();
	affichage();
	return 0;
}

