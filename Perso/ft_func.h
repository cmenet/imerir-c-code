#ifndef __FT_FUNC_H__
#define __FT_FUNC_H__

void	ft_putstr(char *str);
void	ft_itoa(int n);
int		ft_strlen(char *str);
void	ft_putchar(char c);


#endif
