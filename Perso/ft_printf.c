#include "ft_printf.h"
#include "ft_func.h"


int ft_printf(const char* str,...)
{
    int i;
    int li;
    char c;

    char* pt_str;
    va_list ap;


    i   = 0;
    li  = 0;

    va_start(ap,str);

    do
    {
        if(str[i] == '%')
        {
            i++;
            write(1,str+li,(i-1)-li); //affiche une partie la chaine
            li = i + 1;


            if      ( str[ i ] == 'd')
                ft_itoa(va_arg(ap,int));
            else if ( str[ i ] == 'f')
                va_arg(ap,double);
            else if ( str[ i ] == 'c')
            {
                c = va_arg(ap,int); // conversion
                write(1,&c,1);
            }
            else if ( str[ i ] == 's')
            {
                pt_str = va_arg(ap,char*);
                write(1,pt_str, ft_strlen(pt_str));
            }
            else if ( str[ i ] == 'p')
            {
                write(1,"0x",2);
                ft_itoa(va_arg(ap,void*));
            }

        }
        i++;
    }while ( str[i] != '\0');

    write(1,str+li,i-li); //affiche une partie la chaine

    va_end(ap);


    return 1;
}
