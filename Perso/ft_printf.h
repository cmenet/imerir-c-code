#ifndef __FT_PRINTF_H__
#define __FT_PRINTF_H__

#include <stdarg.h>
#include <unistd.h>
#include <stdlib.h>

int ft_printf(const char* str,...);


#endif
