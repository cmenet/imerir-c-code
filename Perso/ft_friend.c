#include <stdio.h>
#include "ft_friend.h"
#include "ft_func.h"
#include "ft_printf.h"


/*
 *plus grand trouvé est pour l'instant
 *11791935 et 9773505
*/
int ft_friend(int nb )
{
   int sdiv;

   sdiv = ft_sdiv(nb);
   if ( nb == ft_sdiv(sdiv))
       return sdiv;
   return -1;
}


int ft_sdiv(int nb)//retourne la somme des diviseurs
{
    int A;
    int B;
    int i;

    A = 1;
    B = 0;

    for( i = 2 ; i <= sqrt(nb) ; i++)
        if ( nb%i == 0)
            A += i + nb/i;


    return A;
}
