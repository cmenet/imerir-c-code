#include <fcntl.h>
#include "ft_func.h"



void	ft_itoa(int n) // pout 10 affiche :
{
    if (n < 0)
    {
        write(1, "-", 1);
        ft_itoa(-1 * n);
    }
    else if (n >= 10)
    {
        ft_itoa(n / 10);
        ft_itoa(n % 10);
    }
    else
        ft_putchar(n + '0');
}




void	ft_putchar(char c)
{
    write(1, &c, 1);
}

int		ft_strlen(char *str)
{
    int i;

    i = 0;
    if (str)
    {
        while (str[i] != '\0')
        {
            i++;
        }
    }
    return (i);
}

void	ft_putstr(char *str)
{
    int i;

    i = 0;
    if (str)
    {
        while (i < ft_strlen(str))
        {
            ft_putchar(str[i]);
        }
    }
}

