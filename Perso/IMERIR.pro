TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    ft_print_alphabet.c \
    ft_printf.c \
    func.c \
    ft_friend.c

HEADERS += \
    ft_func.h \
    ft_friend.h \
    ft_printf.h

