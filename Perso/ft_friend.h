#ifndef __FT_FRIEND_H__
#define __FT_FRIEND_H__

#include <math.h>

int ft_friend(int nb ); // renvoi l'ami d'un nombre ou -1;

int ft_sdiv(int nb);    // renvoie la somme des diviseurs

#endif
