#include <stdio.h>
#include <stdlib.h>

//vide le buffer pour éviter les bug du scanf
void	vider_buffer(void)
{
		char c;
		while ((c = getchar ()) != '\n' && c != EOF);
}

// convertit une valeur entrée en celcius en fahrenheit
float	get_fahrenheit (float celcius)
{
	return (celcius * 9./5. + 32.);
}

// convertit une valeur entrée en fahrenheit en celcius
float	get_celcius (float fahrenheit)
{
	return ((fahrenheit-32) * (5./9.)) ;
}

// affcihe la table de cnversion de -100 a 100 celcius en fahrenheit
void	ft_affiche_table_c(void)
{
	int i;

	for( i = -100 ; i <= 100; i+=20)
	printf("Celcius : %-4.2f Fahrenheit : %-4.2f \n",(float) i ,get_fahrenheit(i) );
}

// affcihe la table de cnversion de -100 a 100 fahrenheit en celcius
void	ft_affiche_table_f(void)
{
	int i;

	for( i = -100 ; i <= 100; i+=20)
	printf("Fahrenheit : %-4.2f Celcius: %-4.2f \n",(float) i ,get_celcius(i) );
}

void	manual(void)
{
	printf( "exo10.out [conversion] <number to convert>\n"\
		"\t  [conversion] <min> <max> <pas>\n"\
		"Convertit on nombre en Celcius ou Fahrenheit\n"\
		"ou affiche une table de conversion\n"\
		"\t [conversion] peut prendre la valeur C pour une conversion en Celcius ou,"\
		"\n\t\t      F pour une conversion en Fahrenheit\n"\
		"\t  <number to convert> est le nombre a convertir\n"\
		"\t  <min> est le minimun de la table de conversion\n"\
		"\t  <max> est le maximum de la table de conversion\n"\
		"\t  <pas> est le pas de la table de conversion\n");

}



// les parrametres passés en argument peuvent etre C ou F ils indiquent le sens de la conversion
int	main(int argc, char** argv)
{
	char	choix;
	float 	nb;

	int	min;
	int	max;
	int	pas;

	int i;// compteur

	if(argc == 2) // affichage de l'aide 
	{
		choix = argv[1][0];
		if( choix == 'H' || choix == 'h')
			manual();
	}

	if (argc == 3) // conversion d'un nombre
	{	

		choix = argv[1][0];
		nb = atof(argv[2]);
		
			
	
			switch (choix)
			{
				case 'F':
				case 'f':   
						
						printf("Conversion en Fahrenheit : %3.2fC° -> %3.2fF° \n", nb, get_fahrenheit(nb));
						break;
		
				case 'C':
				case 'c':  
						
						printf("Conversion en Celcius : %3.2fF° -> %3.2fC° \n", nb, get_celcius(nb));
						break;
				case 3:
						ft_affiche_table_c();
						break;
				case 4:
						ft_affiche_table_f();
						break;

				 
				default: 
						printf("Invalid argument");
						manual();
			}

		return (0);
	}
	else if (argc == 5) // affichage d'une table 
	{
		choix = argv[1][0];
		min = atoi(argv[2]); //min
		max = atoi(argv[3]); //min
		pas = atoi(argv[4]); //min

		if(min < max && min + pas < max)//garde fous
		{

			if (choix == 'C' || choix == 'c')
				for( i = min ; i <= max; i+=pas)
					printf("Fahrenheit : %-10.2fF° Celcius : %-10.2fC° \n",(float) i ,get_celcius(i) );
			else if (choix == 'F' || choix == 'f')
				for( i = min ; i <= max; i+=pas)
					printf("Celcius : %-10.2fC° Fahrenheit : %-10.2fF° \n",(float) i ,get_fahrenheit(i) );

			else { printf("Invalid argument\n"); manual(); }
		}
		else { printf("Invalid argument\n"); manual(); }


	}
	else { printf("Invalid argument\n"); manual(); }

	return (0);
}
