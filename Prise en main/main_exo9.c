#include <stdio.h>
#include <stdlib.h>

/*#define TMIN 0
#define TMAX 240

#define PAS 20*/

//vide le buffer pour éviter les bug du scanf
void vider_buffer()
{
		char c;
		while ((c = getchar ()) != '\n' && c != EOF);
}

// convertit une valeur entrée en celcius en fahrenheit
float get_fahrenheit (float celcius)
{
	return celcius * 9./5. + 32.;
}

// convertit une valeur entrée en fahrenheit en celcius
float get_celcius (float fahrenheit)
{
	return (fahrenheit-32) * (5./9.) ;
}

// affcihe la table de cnversion de -100 a 100 celcius en fahrenheit
void ft_afficheTableC()
{
	int i;

	for( i = -100 ; i <= 100; i+=20)
	printf("Celcius : %-4.2f Fahrenheit : %-4.2f \n",(float) i ,get_fahrenheit(i) );
}

// affcihe la table de cnversion de -100 a 100 fahrenheit en celcius
void ft_afficheTableF()
{
	int i;

	for( i = -100 ; i <= 100; i+=20)
	printf("Fahrenheit : %-4.2f Celcius: %-4.2f \n",(float) i ,get_celcius(i) );
}



// les parrametres passés en argument peuvent etre C ou F ils indiquent le sens de la conversion
int main(int argc, char** argv)
{

	if (argc >=3)
	{	

		char choix = argv[1][0];
		float nb = atof(argv[2]);
	
		do
		{
		

			scanf("%d" , &choix);
			vider_buffer();
	
			switch (choix)
			{
				case 0:
						break;
				case 1:   
						printf("Celcius : ");
						scanf("%f" , &nb);
						//vider_buffer();
						printf("Conversion en Fahrenheit : %3.2f -> %3.2f \n", nb, get_fahrenheit(nb));
						break;
		
				case 2:  
						printf("Fahrenheit : ");
						scanf("%f" , &nb);
						vider_buffer();
						printf("Conversion en Celcius : %3.2f -> %3.2f \n", nb, get_celcius(nb));
						break;
				case 3:
						ft_afficheTableC();
						break;
				case 4:
						ft_afficheTableF();
						break;

				 
				default: printf("Saisie invalide");
			}
		}while( choix != 0);
		
	
		return 0;
	}
}
