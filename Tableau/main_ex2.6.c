#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void ft_print_tab(int * t, int t_size)
{
	int i;
	for ( i = 0; i < t_size; i++ )
		printf("TAB[%d] = %d\n", i ,t[i]);
}

void ft_print_reverse_tab(int * t, int t_size)
{
	int i;
	for( i = t_size-1; i >= 0 ; i--)
		printf("TAB[%d] = %d\n", i ,t[i]);
}

void ft_print_row_tab(int * t, int t_size)
{
	int i;
	for( i = 0; i < t_size; i++)
		printf("TAB[%d] = %5d\t\tTAB[%d] = %5d\n", i ,t[i], t_size-i-1, t[t_size-i-1]);
}

void ft_print_max_tab(int * t, int t_size)
{
	int i;
	int i_max = 0;
	

	for( i = 1 ; i < t_size; i++)
		if ( t[i] > t[i_max])
			i_max = i;

	printf("La valeur max se trouve en %d et vaut %d\n", i_max, t[i_max] );
	
}

void ft_print_min_tab(int * t, int t_size)
{
	int i;
	int i_min = 0;
	

	for( i = 1 ; i < t_size; i++)
		if ( t[i] < t[i_min])
			i_min = i;

	printf("La valeur min se trouve en %d et vaut %d\n", i_min, t[i_min] );
	
}

int ft_min_tab(int * t, int t_size)
{
	int i;
	int i_min = 0;
	

	for( i = 1 ; i < t_size; i++)
		if ( t[i] < t[i_min])
			i_min = i;

	return ( i_min );
	
}

void ft_print_sum_tab(int * t, int t_size)
{
	int i;
	int sum = 0;
	

	for( i = 0 ; i < t_size; i++)
		sum += t[i];

	printf("La somme des valeurs du tableau est : %d\n", sum );
}

int* ft_copy_tab(int * t, int t_size)
{
	int i;
	int* ct = malloc( sizeof ( int ) * t_size);

	for( i = 0 ; i < t_size; i++)
		ct[i] = t[i];

	return ( ct );
}


int * ft_rand_tab(int min, int max, int t_size)
{
	int i;
	int * t = malloc(sizeof (int) * t_size);
	srand(time(NULL));


	for ( i = 0 ; i < t_size; i++)
		t[i] = rand()% (max - min) + min;

	return (t);

	
}

int* ft_short_min_to_max_tab(int * t, int t_size)
{
	int i;
	int j;
	int* t_copy = ft_copy_tab(t,t_size);
	int* t_s;
	int* t_short = malloc( sizeof (int )* t_size);
	int i_min;
	

	for( i = 0 ; i < t_size; i++)
	{
		
		i_min = ft_min_tab(t_copy,t_size-i);
		t_short[i] = t_copy[i_min];

		
		ft_print_tab(t_short,20);
		
		ft_print_tab(t_copy,t_size-i);
		

		if(t_size-i > 1)
		{
			t_s = ft_copy_tab(t_copy,t_size-i);
			//free(t_copy);
			t_copy = malloc( sizeof (int )*(t_size - i -1 ));

			for( j = 0 ; j < t_size ; j++)
				(j < i_min) ? (t_copy[j] = t_s[j]) : (t_copy[j] = t_s[j+1]);
		}

	}

	free(t_s);
	return (t_short);

}





int main (int argc, char** argv )
{
	//declaration
	int tab_v[10][20];
	int tab_max_line[20];
	int tab_max_row[10];
	int i,j;

	int min;
	int max;

	int sum[20];

	srand(time(NULL));


	//initialisation
	for( i = 0 ; i < 10 ; i++)
		for( j = 0 ; j < 20 ; j++)
			tab_v[i][j] = rand()% 1000;
			
	//affichage
	
	/*
	for( i = 0 ; i < 10 ; i++)
	{
		for( j = 0 ; j < 20 ; j++)
			printf("tab[%2d][%2d] = %5d\t",i,j,tab_v[i][j]);
		printf("\n");
	}

	for( j = 0 ; j < 20 ; j++)
	{
		for( i = 0 ; i < 10 ; i++)
			printf("tab[%2d][%2d] = %5d\t",i,j,tab_v[i][j]);
		printf("\n");
	}
	*/	

	min = tab_v[0][0];
	max = tab_v[0][0];		

	for( i = 0 ; i < 10 ; i++)
		for( j = 0 ; j < 20 ; j++)
			if (tab_v[i][j] > max)
				max = tab_v[i][j];
			else if (tab_v[i][j] < min)
				min = tab_v[i][j];

	printf("Le min est %d et le max %d\n", min, max);


	
	for( i = 0 ; i < 10 ; i++)
	{
		max = tab_v[i][0];
		for( j = 0 ; j < 20 ; j++)
			if (tab_v[i][j] > max)
				max = tab_v[i][j];
		
		tab_max_line[i] = max;

	}
	ft_print_tab(tab_max_line,10);
	
	
	for( j = 0 ; j < 20 ; j++)
	{
		max = tab_v[0][j];
		for( i = 0 ; i < 10 ; i++)
			if (tab_v[i][j] > max)
				max = tab_v[i][j];
		
		tab_max_row[j] = max;

	}
	ft_print_tab(tab_max_row,20);



	for( i = 0 ; i < 20 ; i++)
		sum[i] = 0;

	
	for( j = 0 ; j < 20 ; j++)
		for( i = 0 ; i < 10 ; i++)
			sum[j] += tab_v[i][j];

	ft_print_tab(sum, 20);

	/*
	ft_print_tab(tab_v, 20);
	ft_print_reverse_tab(tab_v, 20);
	ft_print_row_tab(tab_v, 20);
	ft_print_max_tab(tab_v, 20);
	ft_print_min_tab(tab_v, 20);
	ft_print_sum_tab(tab_v, 20);

	
	ft_print_tab(ft_copy_tab(tab_v,20),20);
	ft_print_tab(ft_short_min_to_max_tab(tab_v,20),20);

	ft_print_tab(ft_rand_tab(0,100,30),30);
	*/

	return (0);
}
