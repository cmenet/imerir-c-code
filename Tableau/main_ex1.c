#include <stdio.h>
#include <stdlib.h>

//vide le buffer pour éviter les bug du scanf
void	vider_buffer(void)
{
		char c;
		while ((c = getchar ()) != '\n' && c != EOF);
}

// convertit une valeur entrée en celcius en fahrenheit
float	get_fahrenheit (float celcius)
{
	return (celcius * 9./5. + 32.);
}

// convertit une valeur entrée en fahrenheit en celcius
float	get_celcius (float fahrenheit)
{
	return ((fahrenheit-32) * (5./9.)) ;
}

// remplit la table de conversion de min a max celcius en fahrenheit
float**	ft_set_table_f(int min, int max , int pas)
{
	int i;
	float** tab;
	
	if(min < max && min + pas < max)//garde fous
	{
		tab = malloc(sizeof (float*)*2);

		tab[0] = malloc(sizeof (float)* ((max-min)/pas+1));
		tab[1] = malloc(sizeof (float)* ((max-min)/pas+1));

	
		for( i = min  ; i <= max; i+=pas)
		{
			tab[0][i/pas] = i ;
			tab[1][i/pas] = get_fahrenheit(i);
		}
	}

	return (tab);
}

// remplit la table de conversion de min a max fahrenheit en celcius
float**	ft_set_table_c(int min, int max , int pas)
{
	int i;
	float** tab;
	
	if(min < max && min + pas < max)//garde fous
	{
		tab = malloc(sizeof (float*)*2);

		tab[0] = malloc(sizeof (float)* ((max-min)/pas+1));
		tab[1] = malloc(sizeof (float)* ((max-min)/pas+1));

		for( i = min  ; i <= max; i+=pas)
		{
			tab[0][i/pas] = i ;
			tab[1][i/pas] = get_celcius(i);
		}
	}
	
	return (tab);
}
void ft_print_table(float** table, int size)
{
	int i;
	
	for(i = 0; i <= size; i++)
		printf("%-4.2f %-4.2f \n", table[0][i], table[1][i] );

	
}
void	manual(void)
{
	printf( "exo10.out [conversion] <number to convert>\n"\
		"\t  [conversion] <min> <max> <pas>\n"\
		"Convertit on nombre en Celcius ou Fahrenheit\n"\
		"ou affiche une table de conversion\n"\
		"\t [conversion] peut prendre la valeur C pour une conversion en Celcius ou,"\
		"\n\t\t      F pour une conversion en Fahrenheit\n"\
		"\t  <number to convert> est le nombre a convertir\n"\
		"\t  <min> est le minimun de la table de conversion\n"\
		"\t  <max> est le maximum de la table de conversion\n"\
		"\t  <pas> est le pas de la table de conversion\n");

}



// les parrametres passés en argument peuvent etre C ou F ils indiquent le sens de la conversion
int	main(int argc, char** argv)
{
	char	choix;
	float 	nb;

	int	min;
	int	max;
	int	pas;
    int TEST[2][2] = {{0,5},{1,6}};

    printf("%d",TEST[1][0]);

	float**	sup;

	int i;// compteur

	if(argc == 2) // affichage de l'aide 
	{
		choix = argv[1][0];
		if( choix == 'H' || choix == 'h')
			manual();
	}

	if (argc == 3) // conversion d'un nombre
	{	

		choix = argv[1][0];
		nb = atof(argv[2]);
		
			
	
			switch (choix)
			{
				case 'F':
				case 'f':   
						
						printf("Conversion en Fahrenheit : %3.2fC° -> %3.2fF° \n", nb, get_fahrenheit(nb));
						break;
		
				case 'C':
				case 'c':  
						
						printf("Conversion en Celcius : %3.2fF° -> %3.2fC° \n", nb, get_celcius(nb));
						break;
				default: 
						printf("Invalid argument");
						manual();
			}

		return (0);
	}
	else if (argc == 5) // affichage d'une table 
	{
		choix = argv[1][0];
		min = atoi(argv[2]); //min
		max = atoi(argv[3]); //min
		pas = atoi(argv[4]); //min

		if(min < max && min + pas < max)//garde fous
		{

			if (choix == 'C' || choix == 'c')
			{
				sup = ft_set_table_c(min, max, pas);
				ft_print_table(sup,((max-min)/pas));

				free(sup[0]);
				free(sup[1]);
				free(sup);


			}
				
			else if (choix == 'F' || choix == 'f')
			{
				sup = ft_set_table_f(min, max, pas);
				ft_print_table(sup,((max-min)/pas));

				free(sup[0]);
				free(sup[1]);
				free(sup);

	
			}

			else { printf("Invalid argument\n"); manual(); }
		}
		else { printf("Invalid argument\n"); manual(); }


	}
	else { printf("Invalid argument\n"); manual(); }

	return (0);
}
