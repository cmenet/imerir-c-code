#include <stdio.h>
#include <stdlib.h>


void ft_print_tab(int * t, int t_size)
{
	int i;
	for ( i = 0; i < t_size; i++ )
		printf("TAB[%d] = %d\n", i ,t[i]);
}

void ft_print_reverse_tab(int * t, int t_size)
{
	int i;
	for( i = t_size-1; i >= 0 ; i--)
		printf("TAB[%d] = %d\n", i ,t[i]);
}

void ft_print_row_tab(int * t, int t_size)
{
	int i;
	for( i = 0; i < t_size; i++)
		printf("TAB[%d] = %5d\t\tTAB[%d] = %5d\n", i ,t[i], t_size-i-1, t[t_size-i-1]);
}

void ft_print_max_tab(int * t, int t_size)
{
	int i;
	int i_max = 0;
	

	for( i = 1 ; i < t_size; i++)
		if ( t[i] > t[i_max])
			i_max = i;

	printf("La valeur max se trouve en %d et vaut %d\n", i_max, t[i_max] );
	
}

void ft_print_min_tab(int * t, int t_size)
{
	int i;
	int i_min = 0;
	

	for( i = 1 ; i < t_size; i++)
		if ( t[i] < t[i_min])
			i_min = i;

	printf("La valeur min se trouve en %d et vaut %d\n", i_min, t[i_min] );
	
}

int ft_min_tab(int * t, int t_size)
{
	int i;
	int i_min = 0;
	

	for( i = 1 ; i < t_size; i++)
		if ( t[i] < t[i_min])
			i_min = i;

	return ( i_min );
	
}

void ft_print_sum_tab(int * t, int t_size)
{
	int i;
	int sum = 0;
	

	for( i = 0 ; i < t_size; i++)
		sum += t[i];

	printf("La somme des valeurs du tableau est : %d\n", sum );
}

int* ft_copy_tab(int * t, int t_size)
{
	int i;
	int* ct = malloc( sizeof ( int ) * t_size);

	for( i = 0 ; i < t_size; i++)
		ct[i] = t[i];

	return ( ct );
}




int* ft_short_min_to_max_tab(int * t, int t_size)
{
	int i;
	int j;
	int* t_copy = ft_copy_tab(t,t_size);
	int* t_s;
	int* t_short = malloc( sizeof (int )* t_size);
	int i_min;
	

	for( i = 0 ; i < t_size; i++)
	{
		
		i_min = ft_min_tab(t_copy,t_size-i);
		t_short[i] = t_copy[i_min];

		
		ft_print_tab(t_short,20);
		
		ft_print_tab(t_copy,t_size-i);
		

		if(t_size-i > 1)
		{
			t_s = ft_copy_tab(t_copy,t_size-i);
			//free(t_copy);
			t_copy = malloc( sizeof (int )*(t_size - i -1 ));

			for( j = 0 ; j < t_size ; j++)
				(j < i_min) ? (t_copy[j] = t_s[j]) : (t_copy[j] = t_s[j+1]);
		}

	}

	free(t_s);
	return (t_short);

}



int main (int argc, char** argv )
{
	int tab_v[20]= {0,1,2,3,4,5,6,7,8,199,200,11,12,13,-1,15,16,17,18,19};

	ft_print_tab(tab_v, 20);
	ft_print_reverse_tab(tab_v, 20);
	ft_print_row_tab(tab_v, 20);
	ft_print_max_tab(tab_v, 20);
	ft_print_min_tab(tab_v, 20);
	ft_print_sum_tab(tab_v, 20);

	
	ft_print_tab(ft_copy_tab(tab_v,20),20);
	ft_print_tab(ft_short_min_to_max_tab(tab_v,20),20);


	return (0);
}
