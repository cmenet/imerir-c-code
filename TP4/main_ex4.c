#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "polynome.h"


int main (int argc, char** argv)
{
	t_polynome* p = ft_saisie_polynome();
	t_polynome* somme, *produit, *b;
	
	ft_print_polynome(p);

	p = ft_ordonner_polynome(p);

	ft_print_polynome(p);

	printf("valeur du polynome : %d \n" ,ft_calc_polynome(p,2));// x vaut 2	


	

	b = ft_copy_polynome(p);

	printf("valeur du polynome copie\n");
	ft_print_polynome(b);
	printf("valeur du polynome copie FIN\n");
	b = ft_saisie_polynome();


	printf("poly A : \n");
	ft_print_polynome(p);

	printf("poly B : \n");
	ft_print_polynome(b);

	printf("valeur du polynome SOMME\n");
	somme = ft_somme_polynome(p,b);
	ft_print_polynome(somme);


	printf("valeur du polynome PRODUIT\n");
	produit = ft_produit_polynome(p,b);
	ft_print_polynome(produit);


	return 0;
}
