#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#define CARRE(x) ((x)*(x))

/**
*structure representant un point dans un espace vectoriel de 3 dimentions
*/

typedef struct s_point{
	double x;
	double y;
	double z;
} t_point;

/**
*structure representant un vecteur dans un espace vectoriel de 3 dimentions
*b est l'image de a
*/
typedef struct s_vecteur{
	t_point a;
	t_point b;
} t_vecteur;

/**
*structure representant un vecteur colonne dans un espace othonorme
*/

typedef struct s_vecteur_colonne{
	double x;
	double y;
	double z;
} t_vecteur_colonne;



void ft_cleen_buffer()
{
	int c;
	while(( c = getchar()) != '\n' && c != EOF);
}


void ft_print_point(const t_point p)
{
	printf("Point :");
	printf("\tx : %f\n",p.x);
	printf("\ty : %f\n",p.y);
	printf("\tz : %f\n",p.z);
}

void ft_print_vecteur(const t_vecteur v)
{
	printf("Vecteur :\n");

	printf("\tPoint A :\n");
	printf("\t\tx : %f\n",v.a.x);
	printf("\t\ty : %f\n",v.a.y);
	printf("\t\tz : %f\n",v.a.z);

	printf("\tPoint B :\n");
	printf("\t\tx : %f\n",v.b.x);
	printf("\t\ty : %f\n",v.b.y);
	printf("\t\tz : %f\n",v.b.z);
}

void ft_print_vecteur_colonne(const t_vecteur_colonne u)
{
	printf("Vecteur colonne :\n");
	printf("\tx : %f\n",u.x);
	printf("\ty : %f\n",u.y);
	printf("\tz : %f\n",u.z);
}



t_point ft_scanf_point(void)
{
	t_point p;


	
	printf("Saisie d'un point :");
	printf("\tx <- ");
	scanf("%lf", &p.x);
	printf("\ty <- ");
	scanf("%lf", &p.x);
	printf("\tz <- ");
	scanf("%lf", &p.x);
	
}

t_vecteur ft_scanf_vecteur(void)
{
	t_vecteur v;

	printf("Saisie d'un vecteur :\n");
	
	printf("\tsaisie du point A :\n");
	printf("\t\tx <- ");
	scanf("%lf", &v.a.x);

	printf("\t\ty <- ");
	scanf("%lf", &v.a.y);
	printf("\t\tz <- ");
	scanf("%lf", &v.a.z);

	printf("\tsaisie du point B :\n");
	printf("\t\tx <- ");
	scanf("%lf", &v.b.x);
	printf("\t\ty <- ");
	scanf("%lf", &v.b.y);
	printf("\t\tz <- ");
	scanf("%lf", &v.b.z);


	return v;
	
}

t_vecteur_colonne ft_vecteur_colonne(t_vecteur v)
{
	t_vecteur_colonne u;

	u.x = v.b.x - v.a.x;
	u.y = v.b.y - v.a.y;
	u.z = v.b.z - v.a.z;
	
	return (u);
}
 

double ft_norme_vecteur(t_vecteur v)
{
	double s = 0; 
	
	s += CARRE(v.b.x - v.a.x);
	s += CARRE(v.b.y - v.a.y);
	s += CARRE(v.b.z - v.a.z);
	
	s = sqrt(s);

	return (s);
	
}

t_vecteur ft_opposite_vecteur(t_vecteur v)
{
	t_vecteur r;

	r.a.x = v.a.x;
	r.a.y = v.a.y;
	r.a.z = v.a.z;
	
	r.b.x = v.a.x-( v.b.x - v.a.x);
	r.b.y = v.a.y-( v.b.y - v.a.y);
	r.b.z = v.a.z-( v.b.z - v.a.z);

	return (r);
}

t_vecteur_colonne ft_opposite_vecteur_colonne(t_vecteur_colonne v)
{
	t_vecteur_colonne u;

	u.x = v.x * -1;
	u.y = v.y * -1;
	u.z = v.z * -1;

	return(u);
}

t_vecteur_colonne ft_sum_vecteur_colonne(t_vecteur_colonne v, t_vecteur_colonne k)
{
	t_vecteur_colonne u;

	u.x = v.x + k.x;
	u.y = v.y + k.y
	u.z = v.z + k.z;

	return(u);
}

t_vecteur_colonne ft_diff_vecteur_colonne(t_vecteur_colonne v, t_vecteur_colonne k)
{
	t_vecteur_colonne u;

	u.x = v.x - k.x;
	u.y = v.y - k.y
	u.z = v.z - k.z;

	return(u);
}

t_vecteur_colonne ft_diff_vecteur_colonne(t_vecteur_colonne v, t_vecteur_colonne k)
{
	t_vecteur_colonne u;

	u.x = v.x - k.x;
	u.y = v.y - k.y
	u.z = v.z - k.z;

	return(u);
}

t_vecteur_colonne ft_product_vecteur_colonne(t_vecteur_colonne v, double k)
{
	t_vecteur_colonne u;

	u.x = v.x * k;
	u.y = v.y * k;
	u.z = v.z * k;

	return(u);
}


/* return a bollean*/
char ft_product_vecteur_colonne(t_vecteur_colonne v, t_vecteur_colonne u)
{
	if ( (v.x / u.x) == (v.y / u.y) && (v.x / u.x) == (v.z / u.z))
		return (1);
	return (0); 
}




int main(char argc, char** argv)
{

	t_vecteur v, u;
	t_vecteur_colonne c;
	v = ft_scanf_vecteur();
	ft_print_vecteur(v);
	printf("norme : %lf \n", ft_norme_vecteur(v));

	printf("opposite vector\n");
	u = ft_opposite_vecteur(v);
	ft_print_vecteur(u);

	c = ft_vecteur_colonne(v);

	ft_print_vecteur_colonne(c);

	

	return (0); 
}
