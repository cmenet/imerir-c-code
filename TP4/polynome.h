#ifndef POLYNOME_H
#define POLYNOME_H

typedef struct s_monome
{
	unsigned int exposant;
	int coefficient;

} t_monome;


typedef struct s_polynome t_polynome;

struct s_polynome
{
	t_monome *m;
	t_polynome* next;
	
};

t_monome* ft_nouveau(unsigned int ex, int coef);


void ft_print_polynome( t_polynome* p);


void ft_print_polynome_pause( t_polynome* p);


t_polynome* ft_saisie_polynome (void);


int ft_calc_polynome( t_polynome* p , int val );


t_polynome* ft_coeff_polynome(t_polynome* p , int val );


t_polynome* ft_ordonner_polynome (t_polynome* p);



t_polynome* ft_copy_polynome(t_polynome*a);




t_polynome* ft_somme_polynome(t_polynome* a, t_polynome* b);

t_polynome* ft_produit_polynome(t_polynome* a, t_polynome* b);

#endif
