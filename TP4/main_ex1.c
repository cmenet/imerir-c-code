#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#define CARRE(x) ((x)*(x))

/**
*structure representant un point dans un espace vectoriel de 3 dimentions
*/

typedef struct s_point{
	double x;
	double y;
	double z;
} t_point;

/**
*structure representant un vecteur dans un espace vectoriel de 3 dimentions
*b est l'image de a
*/
typedef struct s_vecteur{
	t_point a;
	t_point b;
} t_vecteur;



void ft_cleen_buffer()
{
	int c;
	while(( c = getchar()) != '\n' && c != EOF);
}


void ft_print_point(const t_point p)
{
	printf("Point :");
	printf("\tx : %f\n",p.x);
	printf("\ty : %f\n",p.y);
	printf("\tz : %f\n",p.z);
}

void ft_print_vecteur(const t_vecteur v)
{
	printf("Vecteur :\n");

	printf("\tPoint A :\n");
	printf("\t\tx : %f\n",v.a.x);
	printf("\t\ty : %f\n",v.a.y);
	printf("\t\tz : %f\n",v.a.z);

	printf("\tPoint B :\n");
	printf("\t\tx : %f\n",v.b.x);
	printf("\t\ty : %f\n",v.b.y);
	printf("\t\tz : %f\n",v.b.z);
}



t_point ft_scanf_point(void)
{
	t_point p;


	
	printf("Saisie d'un point :");
	printf("\tx <- ");
	scanf("%lf", &p.x);
	printf("\ty <- ");
	scanf("%lf", &p.x);
	printf("\tz <- ");
	scanf("%lf", &p.x);
	
}

t_vecteur ft_scanf_vecteur(void)
{
	t_vecteur v;

	printf("Saisie d'un vecteur :\n");
	
	printf("\tsaisie du point A :\n");
	printf("\t\tx <- ");
	scanf("%lf", &v.a.x);

	printf("\t\ty <- ");
	scanf("%lf", &v.a.y);
	printf("\t\tz <- ");
	scanf("%lf", &v.a.z);

	printf("\tsaisie du point B :\n");
	printf("\t\tx <- ");
	scanf("%lf", &v.b.x);
	printf("\t\ty <- ");
	scanf("%lf", &v.b.y);
	printf("\t\tz <- ");
	scanf("%lf", &v.b.z);


	return v;
	
}

double ft_norme_vecteur(t_vecteur v)
{
	double s = 0; 
	
	s += CARRE(v.b.x - v.a.x);
	s += CARRE(v.b.y - v.a.y);
	s += CARRE(v.b.z - v.a.z);
	
	s = sqrt(s);

	return (s);
	
}

t_vecteur ft_opposite_vecteur(t_vecteur v)
{
	t_vecteur r;

	r.a.x = v.a.x;
	r.a.y = v.a.y;
	r.a.z = v.a.z;
	
	r.b.x = v.a.x-( v.b.x - v.a.x);
	r.b.y = v.a.y-( v.b.y - v.a.y);
	r.b.z = v.a.z-( v.b.z - v.a.z);

	return (r);
}


int main(char argc, char** argv)
{

	t_vecteur v, u;
	v = ft_scanf_vecteur();
	ft_print_vecteur(v);
	printf("norme : %lf \n", ft_norme_vecteur(v));

	printf("opposite vector\n");
	u = ft_opposite_vecteur(v);
	ft_print_vecteur(u);

	return (0); 
}
