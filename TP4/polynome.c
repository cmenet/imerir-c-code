#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "polynome.h"
 

t_monome* ft_nouveau(unsigned int ex, int coef)
{
	t_monome* m = malloc( sizeof (t_monome) );
	m->exposant = ex;
	m->coefficient = coef;
	
	return m;
}

void ft_print_polynome( t_polynome* p)
{
	if( p == NULL)
	{
		printf( "Affichage polynome NULL\n" );
		return ;
	}
		
	printf( "\x1b[31;6m Affichage polynome : \x1b[4A \n" );

	printf( "%dx^%d", 	p->m->coefficient,
				p->m->exposant );

	p = p->next;

	while(p != NULL)
	{
		printf( " + %dx^%d", 	p->m->coefficient,
					p->m->exposant );
		p = p->next;

	}
	printf("\x1b[0m \n");
	
}

void ft_print_polynome_pause( t_polynome* p)
{
	char a;
	if( p == NULL)
	{
		printf( "Affichage polynome NULL\n" );
		return ;
	}
		
	printf( "Affichage polynome : \n" );

	printf( "%dx^%d", 	p->m->coefficient,
				p->m->exposant );

	p = p->next;

	while(p != NULL)
	{
		scanf("%c",&a);
		printf( " + %dx^%d (next :%p)", p->m->coefficient,
					p->m->exposant,p->next );
		p = p->next;

	}
	printf("\n");
	
}

t_polynome* ft_saisie_polynome (void)
{
	char rep;
	int c;
	int exposant;
	int coefficient;

	t_monome* i;
	t_polynome* p = NULL;
	t_polynome* p_c = NULL;
	t_polynome* p_t;
	
	printf( "Saisie polynome : \n" );
	
	do
	{
		if(p != NULL)
		{
		scanf ("%*[^\n]");
			getchar ();
		}

		printf( "\tSaisie d'un monome (y/n) ? " );

		scanf( "%c", &rep );
		
		
		if(rep == 'y')
		{
		
			printf( "\t\tSaisie du coefficient : " );
			scanf( "%d", &coefficient );
	
			printf( "\t\tSaisie de l'exposant : " );
			scanf( "%d", &exposant );

			i = malloc( sizeof (t_monome));
			i->exposant 	= exposant;
			i->coefficient 	= coefficient;

			p_t = malloc(sizeof (t_polynome));
			p_t->next = NULL;
			p_t->m = i;

			if(p_c != NULL)
			p_c->next = p_t;
	
			p_c = p_t;


			if(p == NULL)
				p = p_c;

		}
		
	}while( rep == 'y' );


	
	
	while ((c = getchar ()) != '\n' && c != EOF);

	return p;
	

}

int ft_calc_polynome( t_polynome* p , int val )
{
	int res = 0;
	t_polynome* t = p;

	while(t != NULL)
	{
		res += t->m->coefficient * pow(val , t->m->exposant);
		t = t->next;
		
	}
	return res;
}

t_polynome* ft_coeff_polynome(t_polynome* p , int val )
{
	t_polynome* t = p;

	while(t != NULL)
	{
		t->m->coefficient = val * t->m->coefficient;
		t = t->next;
	}

	return p;
}

t_polynome* ft_ordonner_polynome (t_polynome* p)
{
	int tmp;
	t_polynome* t = p;
	t_polynome* t2;
	t_polynome* t3; // tmp


	if(p == NULL)
		return NULL;

	
	// meme coefs
	while(t != NULL)
	{
		
		t2 = t;
		while(t2->next != NULL)
		{
			
			if(t->m->exposant == t2->next->m->exposant)
			{
				t->m->coefficient = t->m->coefficient + t2->next->m->coefficient;
				t3 = t2->next;		
				t2->next = t2->next->next;
				free(t3);
			}
			
			if(t2 -> next != NULL)
			t2 = t2->next;

			ft_print_polynome(p);
			ft_print_polynome(t);
			if(t2 != NULL)
			printf("%p\n",t2->next);
			
			
		}
		if(t != NULL)
			t = t->next;
		
	}


	printf("fin arrangement coef\n");
	//ft_print_polynome(p);
	t = p;

	
	//ordo
	while(t->next != NULL)
	{
		
		if(t->next->m->exposant > t->m->exposant)
		{
			tmp = t->m->exposant;

			t->m->exposant = t->next->m->exposant;
			t->next->m->exposant = tmp;

			tmp = t->m->coefficient;
			t->m->coefficient = t->next->m->coefficient;
			t->next->m->coefficient = tmp;

			t = p;
			ft_print_polynome(p);

		}
		else
		t = t->next;
	}


//if(t->m->exposant > t2->next->m->exposant)


	printf("Return ordonne\n");
	return p;
}

t_polynome* ft_copy_polynome(t_polynome*a)
{
	t_polynome* p = NULL;
	t_polynome* p_origin = NULL;
	t_polynome* b = a;
	

	while (b != NULL)
	{
		if(p != NULL)
		{
			p-> next = malloc(sizeof( t_polynome ));
			p = p->next;
		}
		else
		{
			p = malloc(sizeof( t_polynome ));
			p_origin = p;
		}

		p->m = malloc(sizeof(t_monome));

		p->m->coefficient = b->m->coefficient;
		p->m->exposant = b->m->exposant;
		p->next = NULL;
		
			
		b = b -> next;				
	}
	return (p_origin);
	

}



t_polynome* ft_somme_polynome(t_polynome* a, t_polynome* b)
{
	t_polynome* p_o = ft_copy_polynome(a);
	
	t_polynome* p = p_o;
	


	while(p->next != NULL)
		p = p->next;

		p-> next = ft_copy_polynome(b);

		ft_print_polynome(p_o);
		
		
		ft_ordonner_polynome(p_o);

	return p_o;
}
t_polynome* ft_produit_polynome(t_polynome* a, t_polynome* b)
{
	t_polynome* p_a_origin = a;
	t_polynome* p_a = p_a_origin;

	

	t_polynome* p_b_origin = b;
	t_polynome* p_b = p_b_origin;

	t_polynome* p_origin = NULL;
	t_polynome* p = NULL;
	

	while (p_a != NULL)
	{
		while (p_b != NULL)
		{
			if(p != NULL)
			{
				p-> next = malloc(sizeof( t_polynome ));
				p = p->next;
			}
			else
			{
				p = malloc(sizeof( t_polynome ));
				p_origin = p;
			}

			p->m = malloc(sizeof(t_monome));

			p->m->coefficient = p_b->m->coefficient * p_a->m->coefficient;
			p->m->exposant = p_b->m->exposant + p_a->m->exposant;
			p->next = NULL;
		
			
			p_b = p_b -> next;
		}
		p_b = p_b_origin;
		p_a = p_a->next;			
	}

	return p_origin;
}

