
#include "listecell.h"
#include "stdlib.h"

typedef struct ListCellStruct ListCellStruct;

struct ListCellStruct
{
	ListCellStruct* previous;
	ListCellStruct* next;
	void* elem;

} ;



/**
	initialise un cellule données cellule précédente cellule suivante.
*/
ListCell_t  ListCellInit(void *data,ListCell_t prev ,ListCell_t next)
{
	ListCell_t tmp= malloc(sizeof(ListCellStruct));

	if(tmp != NULL)
	{

		tmp->elem = data;
		tmp->previous = prev;
		tmp->next = next;

		if(prev != NULL)
		prev->next = tmp;
		if(next != NULL)
		next->previous = tmp;
	}
	
	return tmp;	
}

ListCell_t ListRelink(ListCell_t plop,ListCell_t a ,ListCell_t b )
{
	plop->previous = a;
	plop->next = b;
	
	return plop;
}




/**
	Supprime un cellule
*/
ListCell_t  ListCellDelete(ListCell_t plop)
{
	if(plop->previous != NULL)
	plop->previous->next = plop->next;
	else return NULL;

	if(plop->next != NULL)
	plop->next->previous = plop->previous;
	else return NULL;

	free(plop);
	
	return plop;

}


/**
	retourne les données de la cellule 
*/
void       *ListCellGetContent(ListCell_t plop)
{
	return plop->elem;
}

/**
	renvoie la cellule suivante
*/
ListCell_t  ListCellGetNext(ListCell_t plop)
{
	return plop->next;
}

/**
	renvoie la cellule précédente
*/
ListCell_t  ListCellGetPrev(ListCell_t plop)
{
	return plop->previous;
}
