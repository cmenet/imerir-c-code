#ifndef __LISTE_H__
#define __LISTE_H__

#include "listecell.h"


/*
 * Definition de Type
 */
#ifndef LIST_T
#define LIST_T
typedef struct ListStruct  * List_t;
#endif




/*
 * Declaration des fonctions
 */

/**
	renvoie une liste initialisée
*/
List_t ListInit(void);

/**
	supprime une liste chainnée
*/
void   ListDelete(List_t);

/**
	renvoie vrai si la liste est vide sinon faux
*/
int    ListIsEmpty(List_t);

/**
	renvoie la taille d'une liste
*/
int    ListSize(List_t);

/**
	renvoie le premier élement d'une liste
*/
int    ListGetFirstElt(List_t,void **);

/**
	renvoie le dernier élément d'un liste 
*/
int    ListGetLastElt(List_t,void **);

/**
	renvoie vrai si l'élément est trouvé
*/
int    ListFindElt(List_t,void *);

/**
	ajoutet l'élément en premier d'une liste
*/
int    ListAddEltFirst(List_t,void *);

/**
	supprime le permier élément d'un liste
*/
int    ListRemFirstElt(List_t);

/**
	ajoute l'éléement en fin de la liste
*/
int    ListAddEltLast(List_t,void *);

/**
	supprime le derneir élément de la liste
*/
int    ListRemLastElt(List_t);

/**
	supprime l'élément
*/
int    ListRemElt(List_t,void *);

/**
	supprime l'élément 
*/
int    ListFindAndRemElt(List_t,void *);

/**
	applique la fonction a l'élément ?
*/
int    ListIter(List_t,int (*)(void *,void *),void *);

#endif

