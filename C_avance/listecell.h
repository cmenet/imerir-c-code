#ifndef __LISTCELL_H__
#define __LISTCELL_H__

/*
 * Definition de Type
 */
typedef struct ListCellStruct * ListCell_t;
#ifndef LIST_T
#define LIST_T
typedef struct ListStruct     * List_t;
#endif



/*
 * Declaration des fonctions
 */

/**
	initialise un céllule données cellule précédente cellule suivante.
*/
ListCell_t  ListCellInit(void *,ListCell_t ,ListCell_t);
/**
	Supprime un cellule
*/
ListCell_t  ListCellDelete(ListCell_t);
/**
	retourne les données de la cellule 
*/
void       *ListCellGetContent(ListCell_t);

/**
	renvoie la cellule suivante
*/
ListCell_t  ListCellGetNext(ListCell_t);

/**
	renvoie la cellule précédente
*/
ListCell_t  ListCellGetPrev(ListCell_t);

/**
	relink une cellule
*/
ListCell_t ListRelink(ListCell_t, ListCell_t ,ListCell_t);

#endif


