#include "stdio.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFFER_SIZE 100


#define ARG_ERROR	1
#define ACCESS_ERROR	2
#define	OPEN_ERROR1	3
#define	OPEN_ERROR2	4
#define STAT_ERROR	5
#define CHMOD_ERROR 	6
#define FCLOSE_ERROR	7
#define COPY_ERROR	8


/*
	echo $? pour voir le retour d'érreur

*/


int main(int argc,char** argv)
{
	// fichiers 
	int in_f;
	int  out_f;

	// adresse fichier en mémoire
	char* m_f;

	// permet de récupérer les statistiques du fichier d'entrée 
	struct stat in_s;
	int copy_length;
 
	if(argc != 3)
		return ARG_ERROR;

	// test que on puisse lire le fichier
	if(access(argv[1],R_OK) != 0)
		return ACCESS_ERROR;

	// ouverture du fichier à copier
	in_f = open(argv[1], O_RDONLY);

	
	if(in_f == -1)
		return OPEN_ERROR1;

	
	
	//ouverture fichier 2
	out_f = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC);

	if(out_f == -1)
		return OPEN_ERROR2;

	// récupère les droits
	if(fstat(in_f,&in_s) < 0)
		return STAT_ERROR; //erreur

	// copie
	
	m_f =  mmap(NULL, in_s.st_size, PROT_READ,
                MAP_PRIVATE, in_f, 0); // offset de zero


	copy_length = write(out_f,m_f,in_s.st_size); // copie  toute la mémoire dans le fichier
	

	if(copy_length != in_s.st_size)// test écriture
		return COPY_ERROR;
		
	//recopie des droits
	if(chmod(argv[2],in_s.st_mode) != 0 )
		return CHMOD_ERROR;

	//fermeture des fichiers
	if(close(in_f) != 0)
		return FCLOSE_ERROR;

	if(close(out_f) != 0)
		return FCLOSE_ERROR;
	
}
