#include "stdio.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define BUFFER_SIZE 100


#define ARG_ERROR	1
#define ACCESS_ERROR	2
#define	FOPEN_ERROR	3
#define STAT_ERROR	4
#define CHMOD_ERROR 	5
#define FCLOSE_ERROR	6

/*
	echo $? pour voir le retour d'érreur

*/



int main(int argc,char** argv)
{
	// fichiers 
	FILE *in_f;
	FILE *out_f;
	
	// permet de récupérer les statistiques du fichier d'entrée 
	struct stat in_s;

	char buffer[BUFFER_SIZE];
 
	if(argc != 3)
		return ARG_ERROR;

	// test que on puisse lire le fichier
	if(access(argv[1],R_OK) != 0)
		return ACCESS_ERROR;

	// ouverture du fichier à copier
	in_f = fopen(argv[1], "r");

	
	if(in_f == NULL)
		return FOPEN_ERROR;

	
	
	//ouverture fichier 2
	out_f = fopen(argv[2], "w+");
	if(out_f == NULL)
		return FOPEN_ERROR;

	// copie
	while(fgets(buffer,BUFFER_SIZE, in_f) != NULL)
		fputs (buffer,out_f);

	// récupère les droits
	if(stat(argv[1],&in_s) < 0)
		return STAT_ERROR; //erreur


	//recopie des droits
	if(chmod(argv[2],in_s.st_mode) != 0 )
		return CHMOD_ERROR;


	//fermeture des fichiers
	if(fclose(in_f) != 0)
		return FCLOSE_ERROR;

	if(fclose(out_f) != 0)
		return FCLOSE_ERROR;
	
}
