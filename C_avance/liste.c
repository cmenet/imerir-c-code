#include "liste.h"
#include "stdlib.h"


typedef struct ListStruct ListStruct;

struct ListStruct
{
	ListCell_t firstElem;
	ListCell_t lastElem;
	
};



/*
 * Declaration des fonctions
 */

/**
	renvoie une liste initialisée
*/
List_t ListInit(void)
{
	List_t plop = malloc( sizeof(  ListStruct ));
	if (plop != NULL)
	{

		plop->firstElem = ListCellInit(NULL, NULL, NULL);
		plop->lastElem = ListCellInit(NULL, NULL, NULL);

		if (plop->firstElem != NULL && plop->lastElem != NULL )
		{
			ListRelink(plop->firstElem,plop->lastElem,plop->lastElem);
			ListRelink(plop->lastElem,plop->firstElem,plop->firstElem);
			return plop;
		}
	}
	return NULL;

	

}

/**
	supprime une liste chainnée
*/
void   ListDelete(List_t wazza)
{
	while(ListCellGetPrev(wazza->lastElem) != wazza->firstElem)
		free( ListCellDelete(ListCellGetPrev(wazza->lastElem)));
}

/**
	renvoie vrai si la liste est vide sinon faux
*/
int    ListIsEmpty(List_t plop)
{
	if (ListCellGetNext(plop->firstElem) == plop->lastElem)
		return 1;
	else
		return 0;
}

/**
	renvoie la taille d'une liste
*/
int    ListSize(List_t plop)
{
	int i;
	ListCell_t p= plop->firstElem;

	for (i = 0 ; p != plop->lastElem; p = ListCellGetNext(p));

	return i;
}

/**
	renvoie le premier élement d'une liste
*/
int    ListGetFirstElt(List_t plop,void ** t)
{
	if(ListCellGetNext(plop->firstElem) != plop->lastElem)
	{
		*t = ListCellGetContent(ListCellGetNext(plop->firstElem));
		return 0;
	}
	


	return 1;
}

/**
	renvoie le dernier élément d'un liste 
*/
int    ListGetLastElt(List_t plop,void **t)
{

	if(ListCellGetPrev(plop->lastElem) != plop->firstElem)
	{
		*t = ListCellGetContent(ListCellGetPrev(plop->lastElem));
		return 0;
	}

	return 0;
}

/**
	renvoie vrai si l'élément est trouvé
*/
int    ListFindElt(List_t plop ,void *elem)
{
	ListCell_t p = plop->firstElem;

	while(p != plop->lastElem)
	{
		if( ListCellGetContent(p)  == elem)
			return 0;

		p = ListCellGetNext(p);
	}
	return 1;
}

/**
	ajoutet l'élément en premier d'une liste
*/
int    ListAddEltFirst(List_t plop ,void *elem)
{
	ListCellInit(elem,plop->firstElem, ListCellGetNext(plop->firstElem));
	return 0;
}

/**
	supprime le permier élément d'un liste
*/
int    ListRemFirstElt(List_t plop)
{
	if(ListCellGetNext(plop->firstElem) != plop->lastElem)
	{
		ListCellDelete( ListCellGetNext(plop->firstElem));
		return 0;
	}
	return 1;
}

/**
	ajoute l'élément en fin de la liste
*/
int    ListAddEltLast(List_t plop,void *data)
{
	ListCellInit(data,ListCellGetPrev(plop->lastElem), plop->lastElem);
	return 0;
}

/**
	supprime le derneir élément de la liste
*/
int    ListRemLastElt(List_t plop)
{
	if(ListCellGetPrev(plop->lastElem) != plop->firstElem)
	{
		ListCellDelete( ListCellGetNext(plop->lastElem));
		return 0;
	}
	return 1;
}

/**
	supprime l'élément
*/
int    ListRemElt(List_t plop ,void * data)
{
	ListCell_t p = plop->firstElem;

	while(p != plop->lastElem)
	{
		if( ListCellGetContent(p) == data)
			{
				ListCellDelete(p);
				return 0;
			}
		p = ListCellGetNext(p);
	}
	return 1;
}

/**
	supprime l'élément 
*/
int    ListFindAndRemElt(List_t plop,void *data)
{
	ListCell_t p = plop->firstElem;

	while(p != plop->lastElem)
	{
		if( ListCellGetContent(p) == data)
			{
				ListCellDelete(p);
				return 0;
			}
		p = ListCellGetNext(p);
	}
	return 1;
}

/**
	applique la fonction a l'élément ?
*/
int    ListIter(List_t,int (*)(void *,void *),void *);
