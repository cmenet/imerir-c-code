#include "liste.h"

#include "stdio.h"

int main(int argc, char** argv)
{

	List_t plop = ListInit();

	int test = ListIsEmpty(plop);

	int a = 66;
	int b = 10;

	ListAddEltFirst(plop, &a);
	ListAddEltFirst(plop, &b);
	ListAddEltFirst(plop, &b);

	test = ListIsEmpty(plop);
	test += 2;	

	int* val;

	printf("val init : %d \n", *val);
	
	if(ListGetFirstElt(plop, (void**)&val  ) == 0 )
	printf("val : %d \n", *val);

	ListRemFirstElt(plop);

	ListRemElt(plop,&b);

	if(ListGetFirstElt(plop, (void**)&val  ) == 0 )
	printf("val : %d \n", *val);

	if(ListGetLastElt(plop, (void**)&val  ) == 0 )
	printf("val2 : %d \n", *val);


	ListFindElt(plop,&b);

	printf("List Size : %d \n", ListSize(plop));
	
	ListAddEltFirst(plop, &a);
	ListAddEltFirst(plop, &b);
	ListAddEltFirst(plop, &b);
	

	ListDelete(plop);
	return 0;
}
